
function generate(){
	var word = "";
    //this sets the variable of word to an empty array
	var string = ["Limbs", "Assessment", "Prosthetics", "Socket", "Conditioning", "Training", "Psychology", "silcone", "Myoeletric", "Devices", "Specialist", "Cosmetic", "Blades", "Replacement"];
	//An array of words relating to the site is created here
    var length = string.length;
	//sets the string for thre characters to be chosen from
	// var letter1 = string.charAt(Math.floor((Math.random() * string.length) + 1), 1);

	word = string[Math.floor(Math.random() * string.length)];
	// sets the variable word to a random word from the string array
	document.getElementById('generated').innerHTML = word;
    // changes the innerHTML of the id 'generated' to the random word
    word = word.toUpperCase();
    //sets the word to uppercase, saved going through them all and changing them

    document.getElementById("captchaForm").onsubmit = function () {
    //when the captcha form is submitted the following happens
    var captchaTry = document.getElementById("captcha").value;
    //this gets the value of what the user typed into the field for the captcha
    if (captchaTry === word) {
        alert('Form Sent');
        //alerts the users that the form is sent if what they typed is the same as the random generated word
    	var change = document.getElementById('generated');
    	change.innerHTML = "Correct";
    	change.style.background = "green";
        //changes the background of the captcha to green and displays correct
    }else{
    	var change = document.getElementById('generated');
    	alert('incorrect');
    	change.innerHTML = "wrong";
    	change.style.background = "red";
        //if the captcha is not correct, the background is changed to red and the innerHTML is set to 'wrong'
    }
  }
}
